"""Set identifier value on contract registry

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
.. pgp:: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746 

"""

# standard imports
import sys
import os
import json
import argparse
import logging

# external imports
import chainlib.eth.cli
from chainlib.chain import ChainSpec
from chainlib.eth.tx import receipt
from chainlib.eth.constant import ZERO_CONTENT
from chainlib.error import JSONRPCException
from chainlib.eth.address import to_checksum_address
from hexathon import (
        add_0x,
        strip_0x,
        )

# local imports
from eth_contract_registry.registry import ContractRegistry

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

arg_flags = chainlib.eth.cli.argflag_std_read | chainlib.eth.cli.Flag.EXEC
argparser = chainlib.eth.cli.ArgumentParser(arg_flags)
argparser.add_argument('identifier', type=str, nargs='?', help='Token symbol to return address for')
args = argparser.parse_args()

extra_args = {
    'identifier': None,
        }
config = chainlib.eth.cli.Config.from_args(args, arg_flags, extra_args=extra_args, default_fee_limit=ContractRegistry.gas())

wallet = chainlib.eth.cli.Wallet()
wallet.from_config(config)

rpc = chainlib.eth.cli.Rpc(wallet=wallet)
conn = rpc.connect_by_config(config)

chain_spec = ChainSpec.from_chain_str(config.get('CHAIN_SPEC'))


def out_element(e, w=sys.stdout):
    if config.get('_RAW'):
        w.write(e[1] + '\n')
    else:
        w.write(e[0] + '\t' + e[1] + '\n')


def element(ifc, conn, registry_address, identifier, w=sys.stdout):
    o = ifc.address_of(registry_address, identifier)
    r =  conn.do(o)
    address = ifc.parse_address_of(r)
    out_element((identifier, address), w)


def ls(ifc, conn, registry_address, w=sys.stdout):
    i = 0
    while True:
        o = ifc.identifier(registry_address, i)
        try:
            r =  conn.do(o)
            identifier = ifc.parse_identifier(r)
            element(ifc, conn, registry_address, identifier, w)
            i += 1
        except JSONRPCException:
            break


def main():
    c = ContractRegistry(chain_spec)

    identifier = config.get('_IDENTIFIER')

    registry_address = to_checksum_address(config.get('_EXEC_ADDRESS'))
    if not config.true('_UNSAFE') and strip_0x(registry_address) != strip_0x(config.get('_EXEC_ADDRESS')):
        raise ValueError('invalid checksum address for contract')

    if identifier != None:
        element(c, conn, registry_address, identifier, w=sys.stdout)
    else:
        ls(c, conn, registry_address, w=sys.stdout)


if __name__ == '__main__':
    main()
